from datetime import date
from typing import Dict
import xml.etree.ElementTree as ET
import requests


def get_currencies(import_date: date) -> Dict:
    ''' '''

    response = requests.get(f'http://www.cbr.ru/scripts/XML_daily.asp?date_req={import_date.strftime("%d/%m/%Y")}')
    response.raise_for_status()
    root = ET.fromstring(response.text)
    res = dict()
    need = ('USD', 'EUR')
    for currency in root:
        code = currency.find('CharCode').text
        if code in need:
            res[code] = currency.find('Value').text.replace(',', '.')
    return res
