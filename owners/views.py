from django.shortcuts import render
from meetings.models import Meeting
from tasks.models import Task
from notices.models import Notice
 
 
def index(request):
    ''' '''
    return render(request, 'index.html', context={
        'next_meeting': Meeting.objects.get_next_for_user(request.user),
        'last_meeting': Meeting.objects.get_last_for_user(request.user),
        'tasks': Task.objects.get_most_important_for_user(request.user),
        'notices': Notice.objects.get_for_user(request.user)[:5],
    })
