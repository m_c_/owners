from collections import namedtuple
import requests
from bs4 import BeautifulSoup


WeatherDto = namedtuple('WeatherDto', ('temperature', 'precipitation', 'description', 'wind'))


def get_weather(url: str) -> WeatherDto:
    ''' '''
    response = requests.get(url)
    response.raise_for_status()
    forecast = BeautifulSoup(response.text, 'html.parser').find('div', 'forecast')
    for day in forecast.find_all('div', 'twelve-hour'):
        part = day.contents[1]
        temperature = None
        precipitation = None
        description = None
        wind = None

        for span in part.find_all('span', 'tip-right'):
            if 'title' in span.attrs:
                if span['title'] == 'Температура воздуха, °C':
                    temperature = span.text
                elif span['title'].startswith('Вероятность осадков:'):
                    tmp = span['title'].split(' ')
                    if len(tmp) > 2:
                        precipitation = int(tmp[2].replace('%', ''))

        tmp = part.find('div', 'weather-text')
        if tmp and tmp.contents[0]:
            description = ' '.join(tmp.contents[0].strings).capitalize()

        tmp = part.find('div', 'wind')
        if tmp and tmp.contents[0]:
            ind = 0
            for st in tmp.contents[0].strings:
                if ind == 1:
                    tmp = st.split(',')
                    wind = tmp[0].capitalize()
                    break
                ind += 1

        return WeatherDto(temperature, precipitation, description, wind)
