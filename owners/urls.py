"""owners URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles import views as static_views
from django.urls import re_path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('documents/', include('documents.urls')),
    path('tasks/', include('tasks.urls')),
    path('workers/', include('workers.urls')),
    path('companies/', include('companies.urls')),
    path('meetings/', include('meetings.urls')),
    path('houses/', include('houses.urls')),
    path('forum/', include('forum.urls')),
    path('notices/', include('notices.urls')),
    path('rules/', admin.site.urls),
    path('users/', include('users.urls')),
    path('', include('social_django.urls', namespace='social'))
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [re_path(r'^static/(?P<path>.*)$', static_views.serve),]
