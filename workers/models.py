from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class WorkerSpecialtyManager(models.Manager):
    ''' '''

    def get_for_user(self, user):
        ''' '''
        if user.is_authenticated and user.has_house():
            return self.filter(workers__city__houses__users=user)
        return self.all()


class WorkerSpecialty(models.Model):
    ''' '''

    title = models.CharField(max_length=255)

    objects = WorkerSpecialtyManager()

    def __str__(self) -> str:
        ''' '''
        return self.title


class WorkerManager(models.Manager):
    ''' '''

    def get_for_user(self, user, spec=None):
        ''' '''
        qs = self.all()
        if spec:
            qs = qs.filter(specialties__id=spec)
        if user.is_authenticated and user.has_house():
            qs = qs.filter(city__houses__users=user)
        return qs.order_by('-rate')


class Worker(models.Model):
    ''' '''

    title = models.CharField(max_length=255)
    phone = models.CharField(max_length=15)
    is_company = models.BooleanField(default=False)
    city = models.ForeignKey('houses.City', models.CASCADE)
    specialties = models.ManyToManyField(WorkerSpecialty, blank=True, related_name='workers')
    rate = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    rated = models.PositiveSmallIntegerField(blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)

    objects = WorkerManager()

    def __str__(self) -> str:
        ''' '''
        return self.title

    def specialties_as_string(self) -> str:
        ''' '''
        return ', '.join(s.title for s in self.specialties.all())


class WorkerReviewManager(models.Manager):
    ''' '''

    def get_for_worker(self, worker: Worker):
        ''' '''
        return self.filter(worker=worker).order_by('-updated_dt')

    def get_rate(self, worker: Worker):
        ''' '''
        res = self.filter(worker=worker).aggregate(models.Count('id', distinct=True), models.Avg('rate'))
        return float(res['rate__avg']), res['id__count'] if res['rate__avg'] else 0, 0


class WorkerReview(models.Model):
    ''' '''

    rate = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    message = models.TextField(blank=True, null=True)
    updated_dt = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    worker = models.ForeignKey(Worker, models.CASCADE, related_name='reviews')

    objects = WorkerReviewManager()

    def __str__(self) -> str:
        ''' '''
        return f'Review {self.id} for worker #{self.worker_id}'
