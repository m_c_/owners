from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from workers import models, forms
from houses.models import City
from utils import views as uviews


class WorkersList(ListView):
    ''' '''

    context_object_name = 'workers'
    model = models.Worker
    template_name = 'workers/list.html'

    def get_queryset(self):
        ''' '''
        spec = self.request.GET.get('specialty', None)
        return self.model.objects.get_for_user(self.request.user, spec)

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['specialties'] = models.WorkerSpecialty.objects.get_for_user(self.request.user)
        return context


class WorkerDetails(DetailView):
    ''' '''

    model = models.Worker
    context_object_name = 'worker'
    template_name = 'workers/details.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['reviews'] = models.WorkerReview.objects.get_for_worker(context[self.context_object_name])
        context['user_review'] = context['reviews'].filter(user=self.request.user).first()
        return context


class WorkerSaveMixin():
    ''' '''

    model = models.Worker
    success_url = reverse_lazy('workers:list')
    template_name = 'workers/form.html'
    form_class = forms.WorkerForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'cities': City.objects.get_for_user(self.request.user)})
        return kwargs


class WorkerAdd(uviews.AddUserMixin, WorkerSaveMixin, CreateView):
    ''' '''


class WorkerUpdate(uviews.IsOwnerMixin, WorkerSaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class WorkerDelete(uviews.IsOwnerMixin, DeleteView):
    ''' '''

    model = models.Worker
    success_url = reverse_lazy('workers:list')


class WorkerReviewSuccessMixin():
    ''' '''

    def get_success_url(self):
        ''' '''
        return reverse('workers:details', args=(self.object.worker_id,))


class WorkerReviewSaveMixin(WorkerReviewSuccessMixin):
    ''' '''

    model = models.WorkerReview
    form_class = forms.WorkerReviewForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'workers': models.Worker.objects.get_for_user(self.request.user)})
        return kwargs


@method_decorator(require_POST, name='dispatch')
class WorkerReviewAdd(uviews.AddUserMixin, WorkerReviewSaveMixin, CreateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class WorkerReviewUpdate(uviews.IsOwnerMixin, WorkerReviewSaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class WorkerReviewDelete(uviews.IsOwnerMixin, WorkerReviewSuccessMixin, DeleteView):
    ''' '''

    model = models.WorkerReview
