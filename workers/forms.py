from django.forms import ModelForm
from utils.forms import FormMixin
from workers import models


class WorkerForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.Worker
        fields = ['title', 'city', 'phone', 'specialties']

    def __init__(self, cities, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['city'].queryset = cities


class WorkerReviewForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.WorkerReview
        fields = ['rate', 'message', 'worker']

    def __init__(self, workers, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['worker'].queryset = workers
