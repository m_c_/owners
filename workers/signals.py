from django.db.models.signals import post_save, post_delete
from workers import models


def update_worker_rate(sender, instance, *args, **kwargs):
    ''' '''
    tmp = models.WorkerReview.objects.get_rate(instance.worker)
    instance.worker.rate, instance.worker.rated = tmp[0], tmp[1]
    instance.worker.save()


post_save.connect(update_worker_rate, models.WorkerReview)
post_delete.connect(update_worker_rate, models.WorkerReview)
