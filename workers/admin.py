from django.contrib import admin
from workers import models


class WorkerAdmin(admin.ModelAdmin):
    ''' '''

    list_filter = ('city',)


admin.site.register(models.Worker, WorkerAdmin)
admin.site.register(models.WorkerReview)
admin.site.register(models.WorkerSpecialty)
