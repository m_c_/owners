from django.urls import path
from workers import views


app_name = 'workers'
urlpatterns = [
    path('', views.WorkersList.as_view(), name='list'),
    path('<int:pk>/', views.WorkerDetails.as_view(), name='details'),
    path('add/', views.WorkerAdd.as_view(), name='add'),
    path('<int:pk>/update/', views.WorkerUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', views.WorkerDelete.as_view(), name='delete'),
    path('reviews/add/', views.WorkerReviewAdd.as_view(), name='review-add'),
    path('reviews/<int:pk>/update/', views.WorkerReviewUpdate.as_view(), name='review-update'),
    path('review/<int:pk>/delete/', views.WorkerReviewDelete.as_view(), name='review-delete'),
]
