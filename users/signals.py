from random import randint
from django.db.models.signals import pre_save
from django.conf import settings


def generate_username(sender, instance, *args, **kwargs):
    ''' '''
    if not instance.username:
        parts = instance.email.split('@')
        add = randint(1, 1000000)
        instance.username = f'{parts[0]}-{add}'


pre_save.connect(generate_username, settings.AUTH_USER_MODEL)
