from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login
from django.views.generic import DetailView, UpdateView
from django.views.generic.edit import CreateView
from django.urls.base import reverse_lazy
from django.shortcuts import redirect
from users import models, forms
from houses.models import House


class UserRegister(CreateView):
    ''' '''

    model = models.User
    success_url = reverse_lazy('index')
    template_name = 'registration/login.html'
    form_class = forms.UserRegisterForm

    def get_context_data(self, **kwargs):
        ''' '''
        context = super().get_context_data(**kwargs)
        context['reg_form'] = context['form']
        del context['form']
        context['link'] = self.request.GET.get('link')
        return context

    def form_valid(self, form):
        ''' '''
        ret = super().form_valid(form)

        if form.cleaned_data['link']:
            house = House.objects.get_for_registration(form.cleaned_data['link'])
            house.users.add(self.object, through_defaults={'is_admin': False})

        user = authenticate(self.request, username=self.object.email, password=form.cleaned_data['password1'])
        if user is not None:
            login(self.request, user)

        return ret

    def get(self, request, *args, **kwargs):
        ''' '''
        if request.user.is_authenticated:
            return redirect('index')
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        ''' '''
        if request.user.is_authenticated:
            return redirect('index')
        return super().post(request, *args, **kwargs)


class UserViewMixin(LoginRequiredMixin):
    ''' '''

    model = models.User
    template_name = 'users/details.html'
    context_object_name = 'user'

    def get_object(self, queryset=None):
        ''' '''
        return self.request.user


class UserDetails(UserViewMixin, DetailView):
    ''' '''


class UserUpdate(UserViewMixin, UpdateView):
    ''' '''

    form_class = forms.UserChangeForm
    success_url = reverse_lazy('users:details')
