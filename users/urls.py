from django.urls import path
from users import views


app_name = 'users'
urlpatterns = [
    path('details/', views.UserDetails.as_view(), name='details'),
    path('update/', views.UserUpdate.as_view(), name='update'),
    path('register/', views.UserRegister.as_view(), name='register'),
]
