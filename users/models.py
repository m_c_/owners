from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin


class UserManager(BaseUserManager):
    ''' '''

    def _create_user(self, email, password, username, is_superuser):
        ''' '''
        if not email:
            raise ValueError('The email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, is_superuser=is_superuser, username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, username):
        ''' '''
        return self._create_user(email, password, username, False)

    def create_superuser(self, email, password, username):
        ''' '''
        return self._create_user(email, password, username, True)

    def get_by_username(self, username):
        ''' '''
        return self.filter(is_active=True,username=username).first()


class User(AbstractBaseUser, PermissionsMixin):
    ''' '''

    email = models.EmailField('Email', unique=True)
    username = models.CharField(max_length=255, unique=True)
    is_active = models.BooleanField('Active', default=True)
    registration_dt = models.DateTimeField('Registation date', auto_now=True)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('username',)

    def is_staff(self) -> bool:
        ''' '''
        return self.is_superuser

    def title(self) -> str:
        ''' '''
        admin = ' (Админ сайта)' if self.is_superuser else ''
        if not admin:
            admin = ' (Админ дома)' if self.is_house_admin() else ''
        return f'{self.username}{admin}'
        
    def has_house_access(self, house) -> bool:
        ''' '''
        return self.memberships.filter(house=house).exists()

    def has_house_admin_access(self, house) -> bool:
        ''' '''
        return self.memberships.filter(house=house,is_admin=True).exists()

    def has_any_house_admin_access(self, houses) -> bool:
        ''' '''
        return self.memberships.filter(house__in=houses,is_admin=True).exists()

    def is_house_admin(self) -> bool:
        ''' '''
        return self.memberships.filter(is_admin=True).exists()

    def has_house(self) -> bool:
        ''' '''
        return self.memberships.exists()

    def get_companies(self):
        ''' '''
        return [h.company for h in self.houses.all()]

    def get_cities(self):
        ''' '''
        return [h.city for h in self.houses.all()]

    def get_timezone(self):
        ''' '''
        for h in self.houses.all():
            return h.city.timezone
        return None

    def get_house_membership(self, house):
        ''' '''
        return self.memberships.filter(house=house).first()
