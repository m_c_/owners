# Generated by Django 3.1.6 on 2021-03-17 03:09

from django.db import migrations, models
import documents.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('category', models.CharField(choices=[('cl', 'Законодательство'), ('ch', 'Домашний')], max_length=2)),
                ('file', models.FileField(blank=True, max_length=255, null=True, upload_to=documents.models.docs_path)),
                ('href', models.URLField(blank=True, max_length=255, null=True)),
                ('uploaded_dt', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
