from django.db import models


def docs_path(instance, filename: str) -> str:
    ''' '''
    if instance.category == Document.CATEGORY_LAW:
        return f'documents/law/{filename}'
    else:
        return f'documents/houses/{instance.house_id}/{filename}'


class DocumentManager(models.Manager):
    ''' '''

    def get_law(self):
        ''' '''
        return self.filter(category=Document.CATEGORY_LAW)

    def get_for_user(self, user):
        ''' '''
        if user.is_authenticated:
            return self.filter(category=Document.CATEGORY_HOUSE, house__users=user).order_by('house', '-uploaded_dt')
        else:
            return tuple()


class Document(models.Model):
    ''' '''

    CATEGORY_LAW = 'cl'
    CATEGORY_HOUSE = 'ch'
    CATEGORY_CHOICES = [(CATEGORY_LAW, 'Законодательство'), (CATEGORY_HOUSE, 'Домашний')]

    title = models.CharField(max_length=255)
    category = models.CharField(max_length=2, choices=CATEGORY_CHOICES)
    file = models.FileField(upload_to=docs_path, max_length=255, blank=True, null=True)
    href = models.URLField(max_length=255, blank=True, null=True)
    uploaded_dt = models.DateTimeField(auto_now_add=True)
    house = models.ForeignKey('houses.House', models.CASCADE, blank=True, null=True)

    objects = DocumentManager()

    def __str__(self) -> str:
        ''' '''
        return self.title

    def has_admin_access(self, user):
        ''' '''
        if not user.is_authenticated:
            return False
        if self.house:
            return user.has_house_admin_access(self.house)
        else:
            return user.is_house_admin()
