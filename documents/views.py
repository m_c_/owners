from django.views.generic import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from documents import models, forms
from houses.models import House 
from utils import views as uviews


class DocumentsList(ListView):
    ''' '''

    context_object_name = 'documents'
    model = models.Document
    template_name = 'documents/list.html'

    def get_queryset(self):
        ''' '''
        return self.model.objects.get_for_user(self.request.user)

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['laws'] = self.model.objects.get_law()
        return context


class DocumentSaveMixin():
    ''' '''

    model = models.Document
    success_url = reverse_lazy('documents:list')
    template_name = 'documents/form.html'
    form_class = forms.DocumentForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'houses': House.objects.get_for_admin_user(self.request.user)})
        return kwargs


class DocumentAdd(uviews.HouseAdminCreateMixin, DocumentSaveMixin, CreateView):
    ''' '''


class DocumentUpdate(uviews.HouseAdminManageMixin, DocumentSaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class DocumentDelete(uviews.HouseAdminManageMixin, DeleteView):
    ''' '''

    model = models.Document
    success_url = reverse_lazy('documents:list')
