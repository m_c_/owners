from django.contrib import admin
from documents import models


class DocumentAdmin(admin.ModelAdmin):
    ''' '''

    list_display = ('title', 'category', 'uploaded_dt')
    list_filter = ('category', 'house')


admin.site.register(models.Document, DocumentAdmin)
