from django.forms import ModelForm
from django.core.exceptions import ValidationError
from utils.forms import FormMixin
from documents import models


class DocumentForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.Document
        fields = ['title', 'category', 'file', 'href', 'house']

    def __init__(self, houses, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['house'].queryset = houses

    def clean(self):
        ''' '''
        cleaned_data = super().clean()

        file = cleaned_data.get('file')
        href = cleaned_data.get('href')
        if not file and not href:
            error = ValidationError('Укажите файл или ссылку')
            self.add_error('file', error)
            self.add_error('href', error)

        category = cleaned_data.get('category')
        house = cleaned_data.get('house')
        if category == models.Document.CATEGORY_HOUSE and not house:
            self.add_error(
                'house', ValidationError('Для этой категории необходимо указать дом')
            )

        return cleaned_data
