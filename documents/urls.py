from django.urls import path
from documents import views


app_name = 'documents'
urlpatterns = [
    path('', views.DocumentsList.as_view(), name='list'),
    path('add/', views.DocumentAdd.as_view(), name='add'),
    path('<int:pk>/update/', views.DocumentUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', views.DocumentDelete.as_view(), name='delete'),
]
