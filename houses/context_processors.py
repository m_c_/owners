from typing import Dict
from django.http import HttpRequest


def house(request: HttpRequest) -> Dict:
    ''' '''

    title, logo, has_house = 'Добро пожаловать', None, False
    if request.user.is_authenticated:
        if request.user.houses.count() == 0:
            pass
        elif request.user.houses.count() == 1:
            title = request.user.houses.first().title
            logo = request.user.houses.first().logo
            has_house = True
        else:
            title = ', '.join(tuple(h.title for h in request.user.houses.all()))
            has_house = True
    return {
        'house_title': title,
        'house_logo': logo,
        'house_registered': has_house
    }
