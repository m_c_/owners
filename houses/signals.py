from django.db.models.signals import post_save, pre_save
from django.utils.crypto import get_random_string
from houses import models
from forum.models import Category


def init_obj(sender, instance, *args, **kwargs):
    ''' '''
    if not instance.reg_link:
        instance.reg_link = get_random_string(19)


def init_relations(sender, instance, created, *args, **kwargs):
    ''' '''
    if created:
        cat = Category(title=f'Форум {instance.forum_title}')
        info = None
        if type(instance) == models.Region:
            cat.region = instance
            info = models.RegionInfo(region=instance)
        elif type(instance) == models.City:
            cat.city = instance
            info = models.CityInfo(city=instance)
        elif type(instance) == models.House:
            cat.house = instance
            info = models.HouseInfo(house=instance)
        cat.save()
        info.save()


pre_save.connect(init_obj, models.House)
post_save.connect(init_relations, models.Region)
post_save.connect(init_relations, models.City)
post_save.connect(init_relations, models.House)
