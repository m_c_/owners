from django.urls import path
from houses import views


app_name = 'houses'
urlpatterns = [
    path('', views.HousesList.as_view(), name='list'),
    path('<int:pk>/update-info/', views.HouseInfoUpdate.as_view(), name='update-info'),
    path('unlink/<int:pk>/', views.UnlinkUser.as_view(), name='user-unlink'),
    path('link/', views.link_user, name='user-link'),
]
