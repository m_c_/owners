from django.views.generic import ListView
from django.views.generic.edit import UpdateView, DeleteView
from django.views.decorators.http import require_POST
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from houses import models
from companies.models import Company
from utils import views as uviews
from users.models import User
from django.shortcuts import redirect


USER_LINK_ERROR_SESSION_KEY = 'user_link_erorr'


class HousesList(uviews.HasHouseMixin, ListView):
    ''' '''

    context_object_name = 'houses'
    model = models.House
    template_name = 'houses/list.html'

    def get_queryset(self):
        ''' '''
        return self.model.objects.get_for_user(self.request.user)

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['companies'] = Company.objects.get_for_user_houses(self.request.user)
        if USER_LINK_ERROR_SESSION_KEY in self.request.session:
            context[USER_LINK_ERROR_SESSION_KEY] = self.request.session[USER_LINK_ERROR_SESSION_KEY]
            del self.request.session[USER_LINK_ERROR_SESSION_KEY]
        return context


class HouseSuccessUrlMixin():
    ''' '''
    
    success_url = reverse_lazy('houses:list')


@method_decorator(require_POST, name='dispatch')
class HouseInfoUpdate(HouseSuccessUrlMixin, uviews.HouseAdminManageMixin, UpdateView):
    ''' '''

    model = models.HouseInfo
    fields = ('info',)


@require_POST
@login_required
def link_user(request):
    ''' '''
    house_id = request.POST.get('house_id')
    username = request.POST.get('username')
    is_admin = request.POST.get('is_admin', False)
    if house_id and username:
        house = models.House.objects.filter(id=house_id).first()
        if house:
            if house.has_admin_access(request.user):
                user = User.objects.get_by_username(username)
                if user:
                    membership = models.HouseMembership(user=user, house=house, is_admin=is_admin)
                    membership.save()
                else:
                    request.session[USER_LINK_ERROR_SESSION_KEY] = 'Пользователь не найден'
            else:
                request.session[USER_LINK_ERROR_SESSION_KEY] = 'Нет прав на добавление пользователя'
        else:
            request.session[USER_LINK_ERROR_SESSION_KEY] = 'Дом не найден'
    else:
        request.session[USER_LINK_ERROR_SESSION_KEY] = 'Укажите дом и пользователя'

    return redirect('houses:list')


@method_decorator(require_POST, name='dispatch')
class UnlinkUser(HouseSuccessUrlMixin, uviews.HouseAdminManageMixin, DeleteView):
    ''' '''

    model = models.HouseMembership
