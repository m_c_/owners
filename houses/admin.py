from django.contrib import admin
from . import models


class RegionInfoInline(admin.StackedInline):
    ''' '''
    model = models.RegionInfo
    can_delete = False
    max_num = 1


class RegionAdmin(admin.ModelAdmin):
    ''' '''
    inlines = [RegionInfoInline]


class CityInfoInline(admin.StackedInline):
    ''' '''
    model = models.CityInfo
    can_delete = False
    max_num = 1


class CityAdmin(admin.ModelAdmin):
    ''' '''
    inlines = [CityInfoInline]
    list_filter = ('region',)


class HouseInfoInline(admin.StackedInline):
    ''' '''
    model = models.HouseInfo
    can_delete = False
    max_num = 1


class HouseMembershipInline(admin.TabularInline):
    ''' '''
    model = models.HouseMembership


class HouseAdmin(admin.ModelAdmin):
    ''' '''
    inlines = [HouseInfoInline, HouseMembershipInline]
    list_filter = ('city',)
    readonly_fields = ('reg_link',)


admin.site.register(models.Region, RegionAdmin)
admin.site.register(models.City, CityAdmin)
admin.site.register(models.House, HouseAdmin)
