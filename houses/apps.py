from django.apps import AppConfig


class HousesConfig(AppConfig):
    ''' '''

    name = 'houses'

    def ready(self):
        ''' '''
        import houses.signals
