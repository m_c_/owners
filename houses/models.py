from django.db import models
from django.conf import settings
from timezone_field import TimeZoneField


class Region(models.Model):
    ''' '''
    
    title = models.CharField(max_length=255, unique=True)
    forum_title = models.CharField(max_length=255)

    def __str__(self) -> str:
        ''' '''
        return self.title


class CityManager(models.Manager):
    ''' '''

    def get_for_user(self, user):
        ''' '''
        if user.is_authenticated and user.has_house():
            return self.filter(houses__users=user)
        return self.all()

    def get_for_weather(self):
        ''' '''
        return self.filter(weather_url__isnull=False)


class City(models.Model):
    ''' '''
    
    title = models.CharField(max_length=100)
    forum_title = models.CharField(max_length=255)
    timezone = TimeZoneField(choices_display='WITH_GMT_OFFSET')
    weather_url = models.URLField(blank=True, null=True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)

    objects = CityManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['title', 'region'], name='city_in_region')
        ]

    def __str__(self) -> str:
        ''' '''
        return self.title


class HouseManager(models.Manager):
    ''' '''

    def get_for_admin_user(self, user):
        ''' '''
        if not user.is_authenticated:
            return tuple()
        return self.filter(memberships__is_admin=True,memberships__user=user)

    def get_for_user(self, user):
        ''' '''
        if not user.is_authenticated:
            return tuple()
        return self.filter(memberships__user=user)

    def get_for_registration(self, link):
        ''' '''
        return self.filter(reg_link=link).first()


class House(models.Model):
    ''' '''

    title = models.CharField(max_length=255)
    forum_title = models.CharField(max_length=255)
    logo = models.ImageField(blank=True, null=True)
    reg_link = models.CharField(max_length=19, unique=True)
    off_link = models.URLField(blank=True, null=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='houses')
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='houses', through='HouseMembership')
    company = models.ForeignKey('companies.Company', on_delete=models.SET_NULL, blank=True, null=True, related_name='houses')

    objects = HouseManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['title', 'city'], name='house_in_city')
        ]

    def __str__(self) -> str:
        ''' '''
        return f'{self.city.title} {self.title}'

    def has_admin_access(self, user) -> bool:
        ''' '''
        if not user.is_authenticated:
            return False
        return user.has_house_admin_access(self)

    def list_users(self):
        ''' '''
        return self.users.all().order_by('username')


class HouseMembership(models.Model):
    ''' '''

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='memberships')
    house = models.ForeignKey(House, on_delete=models.CASCADE, related_name='memberships')
    is_admin = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user', 'house'], name='user_in_house')
        ]

    def __str__(self) -> str:
        ''' '''
        return f'Membership {self.user} in {self.house}'

    def has_admin_access(self, user) -> bool:
        ''' '''
        if not user.is_authenticated:
            return False
        return user.has_house_admin_access(self.house)


class RegionInfo(models.Model):
    ''' '''

    info = models.TextField()
    region = models.OneToOneField(Region, on_delete=models.CASCADE, related_name='info')

    def __str__(self) -> str:
        ''' '''
        return f'Info for {self.region.title}'


class CityInfo(models.Model):
    ''' '''

    info = models.TextField()
    city = models.OneToOneField(City, on_delete=models.CASCADE, related_name='info')

    def __str__(self) -> str:
        ''' '''
        return f'Info for {self.city.title}'


class HouseInfo(models.Model):
    ''' '''

    info = models.TextField()
    house = models.OneToOneField(House, on_delete=models.CASCADE, related_name='info')

    def __str__(self) -> str:
        ''' '''
        return f'Info for {self.house.title}'
