from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from django.urls import reverse
from forum import models, forms
from utils import views as uviews


class CategoriesList(ListView):
    ''' '''

    context_object_name = 'categories'
    model = models.Category
    template_name = 'forum/list.html'

    def get_queryset(self):
        ''' '''
        return self.model.objects.get_for_user(self.request.user)


class CategoryDetails(uviews.ObjectAccessReadMixin, DetailView):
    ''' '''

    context_object_name = 'category'
    model = models.Category
    template_name = 'forum/details.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['adding'] = self.request.GET.get('adding', False)
        return context


class ThreadDetails(uviews.ObjectAccessReadMixin, DetailView):
    ''' '''

    context_object_name = 'thread'
    model = models.Thread
    template_name = 'forum/thread-details.html'


@method_decorator(require_POST, name='dispatch')
class ThreadAdd(uviews.AddUserMixin, CreateView):
    ''' '''
    model = models.Thread
    form_class = forms.ThreadForm
    template_name = 'forum/thread-details.html'

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'categories': models.Category.objects.get_for_user(self.request.user)})
        return kwargs

    def get_success_url(self):
        ''' '''
        return reverse('forum:thread-details', args=(self.object.id,))

    def form_valid(self, form):
        ''' '''
        ret = super().form_valid(form)
        if form.cleaned_data['message']:
            msg = models.Message(
                message = form.cleaned_data['message'],
                thread = form.instance,
                user = form.instance.user
            )
            msg.save()
        return ret 
        

@method_decorator(require_POST, name='dispatch')
class MessageAdd(uviews.AddUserMixin, CreateView):
    ''' '''
    model = models.Message
    form_class = forms.MessageForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'threads': models.Thread.objects.get_for_user(self.request.user)})
        return kwargs

    def get_success_url(self):
        ''' '''
        return reverse('forum:thread-details', args=(self.object.thread_id,))
