from django.forms import ModelForm, CharField
from utils.forms import FormMixin
from forum import models


class ThreadForm(FormMixin, ModelForm):
    ''' '''
    
    message = CharField(required=False)

    class Meta:
        model = models.Thread
        fields = ['title', 'category']

    def __init__(self, categories, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['category'].queryset = categories


class MessageForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.Message
        fields = ['message', 'thread']

    def __init__(self, threads, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['thread'].queryset = threads
