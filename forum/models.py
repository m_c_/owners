from django.db import models
from django.conf import settings


class CategoryManager(models.Manager):
    ''' '''

    def get_for_user(self, user):
        ''' '''
        qs = self.filter(house=None)
        if user.is_authenticated:
            qs = qs | self.filter(house__users=user)
        return qs.order_by('-house', '-city', '-region')


class Category(models.Model):
    ''' '''

    title = models.CharField(max_length=255)
    region = models.OneToOneField('houses.Region', on_delete=models.CASCADE, related_name='forum_category', blank=True, null=True)
    city = models.OneToOneField('houses.City', on_delete=models.CASCADE, related_name='forum_category', blank=True, null=True)
    house = models.OneToOneField('houses.House', on_delete=models.CASCADE, related_name='forum_category', blank=True, null=True)    

    objects = CategoryManager()

    def __str__(self) -> str:
        ''' '''
        return self.title

    def threads_count(self):
        ''' '''
        return self.threads.count()

    def has_access(self, user) -> bool:
        ''' '''
        if self.house:
            return user.has_house_access(self.house) if user.is_authenticated else False
        return True


class ThreadManager(models.Manager):
    ''' '''

    def get_for_user(self, user):
        ''' '''
        qs = self.filter(category__house=None)
        if user.is_authenticated:
            qs = qs | self.filter(category__house__users=user)
        return qs


class Thread(models.Model):
    ''' '''

    title = models.CharField(max_length=255)
    created_dt = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='threads')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)

    objects = ThreadManager()

    def __str__(self) -> str:
        ''' '''
        return self.title

    def messages_count(self):
        ''' '''
        return self.messages.count()

    def has_access(self, user) -> bool:
        ''' '''
        return self.category.has_access(user)


class Message(models.Model):
    ''' '''

    message = models.TextField()
    created_dt = models.DateTimeField(auto_now_add=True)
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE, related_name='messages')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)

    def __str__(self) -> str:
        ''' '''
        return f'Message from thread #{self.thread_id}'
