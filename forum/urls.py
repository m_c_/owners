from django.urls import path
from forum import views


app_name = 'forum'
urlpatterns = [
    path('', views.CategoriesList.as_view(), name='list'),
    path('<int:pk>/', views.CategoryDetails.as_view(), name='details'),
    path('threads/add/', views.ThreadAdd.as_view(), name='thread-add'),
    path('threads/<int:pk>/', views.ThreadDetails.as_view(), name='thread-details'),
    path('messages/add/', views.MessageAdd.as_view(), name='message-add'),
]
