from django.contrib import admin
from forum import models


class ThreadAdmin(admin.ModelAdmin):
    ''' '''

    list_filter = ('category',)


admin.site.register(models.Category)
admin.site.register(models.Thread, ThreadAdmin)
admin.site.register(models.Message)
