function format_time_part(part) {
    return part.toString().padStart(2, '0');
}
function update_time() {
    const d = new Date();
    const curr = [
        format_time_part(d.getDate()),
        format_time_part(d.getMonth()),
        d.getFullYear()
    ].join('.') + ' ' + [
        format_time_part(d.getHours()),
        format_time_part(d.getMinutes())
    ].join(':')
    $('#date').text(curr);
    start_time_display();
}
function start_time_display() {
    tm = setTimeout('update_time()', 10000);
}
$(document).ready(function() {
    update_time();
});