from django.contrib import admin
from meetings import models


class MeetingAdmin(admin.ModelAdmin):
    ''' '''

    list_filter = ('house',)


admin.site.register(models.Meeting, MeetingAdmin)
