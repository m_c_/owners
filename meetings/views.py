from django.views.generic import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from meetings import models, forms
from houses.models import House
from utils import views as uviews


class MeetingsList(uviews.HasHouseMixin, ListView):
    ''' '''

    context_object_name = 'meetings'
    model = models.Meeting
    template_name = 'meetings/list.html'

    def get_queryset(self):
        ''' '''
        return self.model.objects.get_for_user(self.request.user)


class MeetingSaveMixin():
    ''' '''

    model = models.Meeting
    success_url = reverse_lazy('meetings:list')
    template_name = 'meetings/form.html'
    form_class = forms.MeetingForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'houses': House.objects.get_for_admin_user(self.request.user)})
        return kwargs


class MeetingAdd(uviews.HouseAdminCreateMixin, MeetingSaveMixin, CreateView):
    ''' '''


class MeetingUpdate(uviews.HouseAdminManageMixin, MeetingSaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class MeetingDelete(uviews.HouseAdminManageMixin, DeleteView):
    ''' '''

    model = models.Meeting
    success_url = reverse_lazy('meetings:list')
