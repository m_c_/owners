from django.urls import path
from meetings import views


app_name = 'meetings'
urlpatterns = [
    path('', views.MeetingsList.as_view(), name='list'),
    path('add/', views.MeetingAdd.as_view(), name='add'),
    path('<int:pk>/update/', views.MeetingUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', views.MeetingDelete.as_view(), name='delete'),
]
