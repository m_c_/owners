from django.db import models
from django.utils import timezone


def docs_path(instance, filename: str) -> str:
    ''' '''
    dt = instance.appointed_dt.strftime('%Y-%m-%d')
    return f'meetings/{instance.house_id}/{dt}_{filename}'


class MeetingManager(models.Manager):
    ''' '''

    def get_for_user(self, user):
        ''' '''
        if user.is_authenticated:
            return self.filter(house__users=user).order_by('-appointed_dt')
        else:
            return tuple()

    def get_next_for_user(self, user):
        ''' '''
        if not user.is_authenticated:
            return tuple()
        now = timezone.localtime(timezone=user.get_timezone())
        return self.filter(house__users=user,appointed_dt__gt=now).order_by('appointed_dt').first()

    def get_last_for_user(self, user):
        ''' '''
        if not user.is_authenticated:
            return tuple()
        now = timezone.localtime(timezone=user.get_timezone())
        return self.filter(house__users=user,appointed_dt__lte=now).order_by('-appointed_dt').first()


class Meeting(models.Model):
    ''' '''

    appointed_dt = models.DateTimeField()
    appointed_place = models.TextField()
    file = models.FileField(upload_to=docs_path, max_length=255, blank=True, null=True)
    house = models.ForeignKey('houses.House', models.CASCADE)

    objects = MeetingManager()

    def __str__(self):
        ''' '''
        return f'Собрание {self.appointed_dt}'

    def has_admin_access(self, user):
        ''' '''
        if not user.is_authenticated:
            return False
        return user.has_house_admin_access(self.house)
