from django import forms
from utils.forms import FormMixin
from meetings import models


class MeetingForm(FormMixin, forms.ModelForm):
    ''' '''

    appointed_dt = forms.DateTimeField(input_formats=['%d.%m.%Y %H:%M'])

    class Meta:
        model = models.Meeting
        fields = ['appointed_dt', 'appointed_place', 'house']

    def __init__(self, houses, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['house'].queryset = houses
