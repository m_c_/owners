from django.forms import ModelForm
from utils.forms import FormMixin
from notices import models


class NoticeForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.Notice
        fields = ['body', 'house']

    def __init__(self, houses, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['house'].queryset = houses
