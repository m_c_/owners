from django.db import models
from django.conf import settings


class NoticeManager(models.Manager):
    ''' '''

    def get_for_user(self, user):
        ''' '''
        if user.is_authenticated and user.has_house():
            qs = self.filter(house__users=user) | self.filter(house__isnull=True)
        else:
            qs = self.filter(house__isnull=True)
        return qs.order_by('-created_dt')


class Notice(models.Model):
    ''' '''

    body = models.TextField()
    created_dt = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    house = models.ForeignKey('houses.House', models.CASCADE, blank=True, null=True)

    objects = NoticeManager()

    def __str__(self):
        ''' '''
        return f'Notice #{self.id}'
