from django.urls import path
from notices import views


app_name = 'notices'
urlpatterns = [
    path('', views.NoticesList.as_view(), name='list'),
    path('add/', views.NoticeAdd.as_view(), name='add'),
    path('<int:pk>/update/', views.NoticeUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', views.NoticeDelete.as_view(), name='delete'),
]
