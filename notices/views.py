from django.views.generic import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from notices import models, forms
from houses.models import House
from utils import views as uviews


class NoticesList(ListView):
    ''' '''

    context_object_name = 'notices'
    model = models.Notice
    template_name = 'notices/list.html'

    def get_queryset(self):
        ''' '''
        return self.model.objects.get_for_user(self.request.user)


class NoticeSaveMixin():
    ''' '''

    model = models.Notice
    success_url = reverse_lazy('notices:list')
    template_name = 'notices/form.html'
    form_class = forms.NoticeForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'houses': House.objects.get_for_user(self.request.user)})
        return kwargs


class NoticeAdd(uviews.AddUserMixin, NoticeSaveMixin, CreateView):
    ''' '''


class NoticeUpdate(uviews.IsOwnerMixin, NoticeSaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class NoticeDelete(uviews.IsOwnerMixin, DeleteView):
    ''' '''

    model = models.Notice
    success_url = reverse_lazy('notices:list')
