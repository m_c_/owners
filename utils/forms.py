class FormMixin():
    ''' '''

    def __init__(self, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        for _, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = 'Обязательное поле'
