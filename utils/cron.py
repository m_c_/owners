from decimal import Decimal
from django.utils.timezone import now
from django_cron import CronJobBase, Schedule
from utils import models
from owners.cbr import get_currencies
from houses.models import City
from owners.primpogoda import get_weather


class CurrencyCronJob(CronJobBase):
    ''' '''

    RUN_EVERY_MINS = 1440 # every 24 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'utils.currency_cron_job' # a unique code

    def do(self):
        ''' '''
        for code, val in get_currencies(now()).items():
            try:
                curr = models.Currency.objects.get(code=code)
            except models.Currency.DoesNotExist:
                curr = models.Currency(code=code)
            curr.price = Decimal(val)
            curr.save()


class WeatherCronJob(CronJobBase):
    ''' '''

    RUN_EVERY_MINS = 1440 # every 24 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'utils.weather_cron_job' # a unique code

    def do(self):
        ''' '''
        for city in City.objects.get_for_weather():
            wthr = get_weather(city.weather_url)
            if not wthr.temperature or not wthr.precipitation or not wthr.description or not wthr.wind:
                continue

            try:
                weather = city.weather
            except City.weather.RelatedObjectDoesNotExist:
                weather = models.Weather(city=city)

            weather.temperature = wthr.temperature
            weather.precipitation = wthr.precipitation
            weather.description = wthr.description
            weather.wind = wthr.wind
            weather.save()
