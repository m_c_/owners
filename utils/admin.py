from django.contrib import admin
from utils import models


admin.site.register(models.Currency)
admin.site.register(models.Weather)
