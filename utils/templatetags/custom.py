from django import template
from django.utils import timezone


register = template.Library()


@register.filter(name='object_can_admin')
def object_can_admin(obj, user) -> bool:
    ''' '''
    return obj.has_admin_access(user) 


@register.filter(name='change_tz')
def change_tz(dt, user):
    ''' '''
    if not user.is_authenticated:
        return dt
    tz = user.get_timezone()
    return timezone.localtime(dt, tz) if tz else dt


@register.filter(name='get_membership')
def get_membership(user, house):
    ''' '''
    if user.is_authenticated:
        return user.get_house_membership(house)
    return None
