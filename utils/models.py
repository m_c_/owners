from django.db import models
from houses.models import City


class Currency(models.Model):
    ''' '''

    code = models.CharField(max_length=3, unique=True)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    updated_dt = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        ''' '''
        return self.code


class WeatherManager(models.Manager):
    ''' '''

    def get_one_for_user(self, user):
        ''' '''
        if not user.is_authenticated:
            return None
        qs = self.filter(city__houses__users=user)
        if qs.count() == 1:
            return qs.first()
        return None


class Weather(models.Model):
    ''' '''

    temperature = models.CharField(max_length=50)
    precipitation = models.SmallIntegerField()
    wind = models.CharField(max_length=50)
    description = models.CharField(max_length=255)
    updated_dt = models.DateTimeField(auto_now=True)
    city = models.OneToOneField(City, on_delete=models.CASCADE, related_name='weather')

    objects = WeatherManager()

    def __str__(self) -> str:
        ''' '''
        return f'Weather for {self.city_id}'
