from typing import Dict
from django.http import HttpRequest
from utils import models


def info(request: HttpRequest) -> Dict:
    ''' '''
    return {
        'currencies': models.Currency.objects.all(),
        'weather': models.Weather.objects.get_one_for_user(request.user),
    }
