from django.http import Http404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


class HouseAdminCreateMixin(LoginRequiredMixin, UserPassesTestMixin):
    ''' for CreateView '''

    def test_func(self):
        ''' '''
        return self.request.user.is_house_admin()


class HouseAdminManageMixin(LoginRequiredMixin):
    ''' for UpdateView and DeleteView '''

    def get_object(self, queryset=None):
        ''' '''
        obj = super().get_object(queryset)
        if not obj.has_admin_access(self.request.user):
            raise Http404()
        return obj


class ObjectAccessReadMixin():
    ''' for DetailView '''

    def get_object(self, queryset=None):
        ''' '''
        obj = super().get_object(queryset)
        if not obj.has_access(self.request.user):
            raise Http404()
        return obj


class HasHouseMixin(LoginRequiredMixin, UserPassesTestMixin):
    ''' '''

    def test_func(self):
        ''' '''
        return self.request.user.has_house()


class AddUserMixin(LoginRequiredMixin):
    ''' '''

    def form_valid(self, form):
        ''' '''
        form.instance.user = self.request.user
        return super().form_valid(form)


class IsOwnerMixin(LoginRequiredMixin):
    ''' '''

    def get_object(self, queryset=None):
        ''' '''
        obj = super().get_object(queryset)
        if self.request.user != obj.user:
            raise Http404()
        return obj
