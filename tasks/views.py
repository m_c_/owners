from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from django.contrib.auth.mixins import LoginRequiredMixin
from tasks import models, forms
from houses.models import House
from utils import views as uviews


class TasksList(uviews.HasHouseMixin, ListView):
    ''' '''

    context_object_name = 'tasks'
    model = models.Task
    template_name = 'tasks/list.html'

    def get_queryset(self):
        ''' '''
        return self.model.objects.get_for_user(self.request.user)


class TaskDetails(LoginRequiredMixin, uviews.ObjectAccessReadMixin, DetailView):
    ''' '''

    model = models.Task
    context_object_name = 'task'
    template_name = 'tasks/details.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['user_rate'] = models.TaskRate.objects.filter(user=self.request.user).first()
        return context


class TaskSaveMixin():
    ''' '''

    model = models.Task
    template_name = 'tasks/form.html'
    form_class = forms.TaskForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'houses': House.objects.get_for_user(self.request.user)})
        return kwargs


class TaskAdd(uviews.HasHouseMixin, TaskSaveMixin, uviews.AddUserMixin, CreateView):
    ''' '''

    success_url = reverse_lazy('tasks:list')


class TaskUpdate(uviews.IsOwnerMixin, TaskSaveMixin, UpdateView):
    ''' '''

    def get_success_url(self):
        ''' '''
        return reverse('tasks:details', args=(self.object.id,))


@method_decorator(require_POST, name='dispatch')
class TaskDelete(uviews.IsOwnerMixin, DeleteView):
    ''' '''

    model = models.Task
    success_url = reverse_lazy('tasks:list')


class TaskRelationsMixin():
    ''' '''

    def get_success_url(self):
        ''' '''
        return reverse('tasks:details', args=(self.object.task_id,))

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'tasks': models.Task.objects.get_for_user(self.request.user)})
        return kwargs


class TaskRateSaveMixin(TaskRelationsMixin):
    ''' '''

    model = models.TaskRate
    form_class = forms.TaskRateForm


@method_decorator(require_POST, name='dispatch')
class TaskRateAdd(uviews.HasHouseMixin, uviews.AddUserMixin, TaskRateSaveMixin, CreateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class TaskRateUpdate(uviews.IsOwnerMixin, TaskRateSaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class TaskMessageAdd(uviews.HasHouseMixin, uviews.AddUserMixin, TaskRelationsMixin, CreateView):
    ''' '''

    model = models.TaskMessage
    form_class = forms.TaskMessageForm
