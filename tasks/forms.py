from django.forms import ModelForm
from utils.forms import FormMixin
from tasks import models


class TaskForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.Task
        fields = ['title', 'description', 'house']

    def __init__(self, houses, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['house'].queryset = houses


class TaskRateForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.TaskRate
        fields = ['rate', 'task']

    def __init__(self, tasks, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['task'].queryset = tasks


class TaskMessageForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.TaskMessage
        fields = ['message', 'task']

    def __init__(self, tasks, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['task'].queryset = tasks
