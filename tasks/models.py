from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.conf import settings


class TaskManager(models.Manager):
    ''' '''

    def get_for_user(self, user):
        ''' '''
        if user.is_authenticated:
            return self.filter(house__users=user).order_by('-rate')
        else:
            return tuple() 

    def get_most_important_for_user(self, user):
        ''' '''
        res = self.get_for_user(user)
        return res if type(res) == tuple else res[:5] 


class Task(models.Model):
    ''' '''

    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    created_dt = models.DateTimeField(auto_now_add=True)
    updated_dt = models.DateTimeField(auto_now=True)
    rate = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    rated = models.PositiveSmallIntegerField(blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    house = models.ForeignKey('houses.House', models.CASCADE)

    objects = TaskManager()

    def __str__(self) -> str:
        ''' '''
        return self.title

    def has_access(self, user) -> bool:
        ''' '''
        if not user.is_authenticated:
            return False
        return user.has_house_access(self.house)


class TaskRateManager(models.Manager):
    ''' '''

    def get_rate(self, task: Task):
        ''' '''
        res = self.filter(task=task).aggregate(models.Count('id', distinct=True), models.Avg('rate'))
        return float(res['rate__avg']), res['id__count'] if res['rate__avg'] else 0, 0


class TaskRate(models.Model):
    ''' '''

    rate = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    updated_dt = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    task = models.ForeignKey(Task, models.CASCADE, related_name='rates')

    objects = TaskRateManager()

    def __str__(self) -> str:
        ''' '''
        return f'Rate {self.id} for task #{self.task_id}'


class TaskMessage(models.Model):
    ''' '''

    message = models.TextField()
    created_dt = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    task = models.ForeignKey(Task, models.CASCADE, related_name='messages')

    def __str__(self) -> str:
        ''' '''
        return f'Message {self.id} for task #{self.task_id}'
