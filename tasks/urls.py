from django.urls import path
from tasks import views


app_name = 'tasks'
urlpatterns = [
    path('', views.TasksList.as_view(), name='list'),
    path('<int:pk>/', views.TaskDetails.as_view(), name='details'),
    path('add/', views.TaskAdd.as_view(), name='add'),
    path('<int:pk>/update/', views.TaskUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', views.TaskDelete.as_view(), name='delete'),
    path('rate/add/', views.TaskRateAdd.as_view(), name='rate-add'),
    path('rate/<int:pk>/update/', views.TaskRateUpdate.as_view(), name='rate-update'),
    path('message/add/', views.TaskMessageAdd.as_view(), name='message-add'),
]
