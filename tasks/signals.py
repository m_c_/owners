from django.db.models.signals import post_save, post_delete
from tasks import models


def update_task_rate(sender, instance, *args, **kwargs):
    ''' '''
    tmp = models.TaskRate.objects.get_rate(instance.task)
    instance.task.rate, instance.task.rated = tmp[0], tmp[1]
    instance.task.save()


post_save.connect(update_task_rate, models.TaskRate)
post_delete.connect(update_task_rate, models.TaskRate)
