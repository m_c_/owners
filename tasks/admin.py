from django.contrib import admin
from tasks import models


class TaskAdmin(admin.ModelAdmin):
    ''' '''

    list_filter = ('house',)


admin.site.register(models.Task, TaskAdmin)
admin.site.register(models.TaskRate)
admin.site.register(models.TaskMessage)
