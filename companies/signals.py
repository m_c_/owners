from django.db.models.signals import post_save, post_delete
from companies import models


def update_company_rate(sender, instance, *args, **kwargs):
    ''' '''
    tmp = models.CompanyReview.objects.get_rate(instance.company)
    instance.company.rate, instance.company.rated = tmp[0], tmp[1]
    instance.company.save()


post_save.connect(update_company_rate, models.CompanyReview)
post_delete.connect(update_company_rate, models.CompanyReview)
