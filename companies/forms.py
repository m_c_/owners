from django.forms import ModelForm
from utils.forms import FormMixin
from companies import models


class CompanyForm(FormMixin, ModelForm):
    ''' '''

    class Meta:
        model = models.Company
        fields = ['title', 'address', 'href', 'city', 'phone', 'email']

    def __init__(self, cities, *args, **kwargs):
        ''' '''
        super().__init__(*args, **kwargs)
        self.fields['city'].queryset = cities
