from django.contrib import admin
from companies import models


class CompanyAdmin(admin.ModelAdmin):
    ''' '''

    list_filter = ('city',)


admin.site.register(models.Company, CompanyAdmin)
admin.site.register(models.CompanyReview)
