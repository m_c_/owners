from django.urls import path
from companies import views


app_name = 'companies'
urlpatterns = [
    path('', views.CompaniesList.as_view(), name='list'),
    path('<int:pk>/', views.CompanyDetails.as_view(), name='details'),
    path('add/', views.CompanyAdd.as_view(), name='add'),
    path('<int:pk>/update/', views.CompanyUpdate.as_view(), name='update'),
    path('<int:pk>/delete/', views.CompanyDelete.as_view(), name='delete'),
    path('reviews/add/', views.CompanyReviewAdd.as_view(), name='review-add'),
    path('reviews/<int:pk>/update/', views.CompanyReviewUpdate.as_view(), name='review-update'),
    path('review/<int:pk>/delete/', views.CompanyReviewDelete.as_view(), name='review-delete'),
]
