from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class CompanyManager(models.Manager):
    ''' '''

    def get_for_user_houses(self, user):
        ''' '''
        if user.is_authenticated:
            return self.filter(city__in=user.get_cities()).order_by('title')
        return tuple()


class Company(models.Model):
    ''' '''

    title = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=15)
    href = models.URLField(max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=255, blank=True, null=True)
    rate = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    rated = models.PositiveSmallIntegerField(blank=True, null=True)
    city = models.ForeignKey('houses.City', models.CASCADE)

    objects = CompanyManager()

    def __str__(self) -> str:
        ''' '''
        return self.title

    def has_admin_access(self, user) -> bool:
        ''' '''
        if user.is_authenticated:
            return user.has_any_house_admin_access(self.houses.all())
        return False


class CompanyReviewManager(models.Manager):
    ''' '''

    def get_for_company(self, company: Company):
        ''' '''
        return self.filter(company=company).order_by('-updated_dt')

    def get_rate(self, company: Company):
        ''' '''
        res = self.filter(company=company).aggregate(models.Count('id', distinct=True), models.Avg('rate'))
        return float(res['rate__avg']), res['id__count'] if res['rate__avg'] else 0, 0


class CompanyReview(models.Model):
    ''' '''

    rate = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    message = models.TextField(blank=True, null=True)
    updated_dt = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    company = models.ForeignKey(Company, models.CASCADE, related_name='reviews')

    objects = CompanyReviewManager()

    def __str__(self) -> str:
        ''' '''
        return f'Review {self.id} for company #{self.company_id}'
