from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from companies import models, forms
from houses.models import City
from utils import views as uviews


class CompaniesList(ListView):
    ''' '''

    context_object_name = 'companies'
    model = models.Company
    template_name = 'companies/list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['user_companies'] = self.request.user.get_companies() if self.request.user.is_authenticated else []
        return context


class CompanyDetails(DetailView):
    ''' '''

    model = models.Company
    context_object_name = 'company'
    template_name = 'companies/details.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        ''' '''
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['reviews'] = models.CompanyReview.objects.get_for_company(context[self.context_object_name])
        context['user_review'] = context['reviews'].filter(user=self.request.user).first()
        return context


class CompanySaveMixin():
    ''' '''

    model = models.Company
    template_name = 'companies/form.html'
    form_class = forms.CompanyForm

    def get_form_kwargs(self):
        ''' '''
        kwargs = super().get_form_kwargs()
        kwargs.update({'cities': City.objects.get_for_user(self.request.user)})
        return kwargs

    def get_success_url(self):
        ''' '''
        return reverse('companies:details', args=(self.object.id,))


class CompanyAdd(uviews.HouseAdminCreateMixin, CompanySaveMixin, CreateView):
    ''' '''


class CompanyUpdate(uviews.HouseAdminManageMixin, CompanySaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class CompanyDelete(uviews.HouseAdminManageMixin, DeleteView):
    ''' '''

    model = models.Company
    success_url = reverse_lazy('companies:list')


class CompanyReviewSuccessMixin():
    ''' '''

    def get_success_url(self):
        ''' '''
        return reverse('companies:details', args=(self.object.company_id,))


class CompanyReviewSaveMixin(CompanyReviewSuccessMixin):
    ''' '''

    model = models.CompanyReview
    fields = ['rate', 'message', 'company']


@method_decorator(require_POST, name='dispatch')
class CompanyReviewAdd(uviews.AddUserMixin, CompanyReviewSaveMixin, CreateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class CompanyReviewUpdate(uviews.IsOwnerMixin, CompanyReviewSaveMixin, UpdateView):
    ''' '''


@method_decorator(require_POST, name='dispatch')
class CompanyReviewDelete(uviews.IsOwnerMixin, CompanyReviewSuccessMixin, DeleteView):
    ''' '''

    model = models.CompanyReview
